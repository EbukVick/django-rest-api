from rest_framework import serializers

from quiz.models import Answer, Question, Quizzes


# create serializers here
class QuizSerializer(serializers.ModelSerializer):

    class Meta:
        model = Quizzes
        fields = [
            'title',
        ]


class AnswerSerializer(serializers.ModelSerializer):


    class Meta:
        model = Answer

        fields = [
            'id',
            'answer_text',
            'is_right'
        ]


class RandomQuestionSerializer(serializers.ModelSerializer):

    #  This serializer will satisfy for both the QuizQuestion and RandomQuiz view
    answer = AnswerSerializer(many=True, read_only=True)

    class Meta:
        model = Question
        fields = [
            'title', 'answer',
        ]


class QuizQuestionSerializer(serializers.ModelSerializer):

    #  This serializer will satisfy for both the QuizQuestion and RandomQuiz view
    answer = AnswerSerializer(many=True, read_only=True)
    quiz = QuizSerializer(read_only=True)

    class Meta:
        model = Question
        fields = [
           'quiz', 'title', 'answer',
        ]
