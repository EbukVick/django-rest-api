from django.urls import path
from .views import Quiz, QuizQuestion, RandomQuestion

app_name = 'quiz'

urlpatterns = [
    path('', Quiz.as_view(), name='quiz'),

    # For random questions in a particular quiz
    path('random/<str:topic>/', RandomQuestion.as_view(), name='random'),

    # For all questions in a particular quiz
    path('questions/<str:topic>/', QuizQuestion.as_view(), name='questions')
]
