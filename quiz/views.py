from rest_framework import generics
from  .models import Question, Quizzes
from .serializers import QuizQuestionSerializer, QuizSerializer, RandomQuestionSerializer
from rest_framework.views import APIView
from rest_framework.response import Response

# Create your views here.
class Quiz(generics.ListAPIView):
    serializer_class = QuizSerializer
    queryset = Quizzes.objects.all()

class RandomQuestion(APIView):
    def get(self, request, format=None, **kwargs):
        question = Question.objects.filter(quiz__title=kwargs['topic']).order_by('?')[:1]
        serializer = RandomQuestionSerializer(question, many=True)

        return Response(serializer.data)


#  To get all the questions in a particular quizz
class QuizQuestion(APIView):

    def get(self, request, format=None, **kwargs):
        question = Question.objects.filter(quiz__title=kwargs['topic']).order_by('id').all()
        serializer = QuizQuestionSerializer(question, many=True)

        return Response(serializer.data)